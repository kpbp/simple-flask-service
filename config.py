import os
basedir = os.path.dirname(__file__)

class Config(object):
    CURRENT_PATH = os.path.dirname(os.path.abspath(__file__))
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'flask-service-key-789789789'
    STATIC_DIR = os.path.join(CURRENT_PATH, 'app/static/')