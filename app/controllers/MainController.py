from flask import Flask, render_template, send_from_directory
from app import app

@app.route('/')
@app.route('/index')
@app.route('/main')
def Main():
    return render_template('index.j2', title='Тестовая страница', pagename='Это работает!')

@app.route('/sample')
@app.route('/sample/')
@app.route('/sample/<var>')
def SamplePage(var=None):
    return render_template('sample.j2', title='Вторая тестовая страница', pagename='Это вторая тестовая страница', var=var)

@app.errorhandler(404)
def Error404(error):
    return render_template('404.j2', title='Ошибка 404', pagename='Ошибка 404')

@app.route('/<path:path>')
def Static(path):
    return send_from_directory(app.config['STATIC_DIR'], path)